import {
    Scene,
    PerspectiveCamera,
    WebGLRenderer,
    BoxGeometry,
    MeshBasicMaterial,
    Mesh,
    PlaneGeometry,
    DoubleSide,
    Color,
    CylinderGeometry,
    Raycaster,
    Vector2,
    Object3D,
    Geometry,
    AmbientLight,
    HemisphereLight,
    MeshLambertMaterial,
    PointLight,
    MeshPhongMaterial
} from 'three';

import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls'


let scene = new Scene()
let camera = new PerspectiveCamera()
let renderer = new WebGLRenderer()

camera.aspect = window.innerWidth / window.innerHeight;
camera.updateProjectionMatrix();
renderer.setSize(window.innerWidth, window.innerHeight)
window.addEventListener('resize', ev => {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight)
}, false);

renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild( renderer.domElement );

const controls = new OrbitControls(camera, renderer.domElement);
controls.enableZoom = true;
camera.position.z = 10;
camera.position.y = 5
camera.lookAt(0,0,0)


let board_geometry = new PlaneGeometry(8,8,8,8);
let board_materials = [];
board_materials.push(new MeshLambertMaterial({color: 0xffffff, side: DoubleSide}))
board_materials.push(new MeshLambertMaterial({color: 0x000000, side: DoubleSide}))

//Colour faces

let squares = board_geometry.faces.length / 2;
for (let i = 0; i < squares; i++){
    let j = i * 2;
    board_geometry.faces[j].materialIndex = (i + Math.floor(i/8)) % 2;
    board_geometry.faces[j+1].materialIndex = (i + Math.floor(i/8)) % 2;
}


board_geometry.rotateX(Math.PI / 2)
let board = new Mesh( board_geometry, board_materials );
scene.add( board );

scene.add(new AmbientLight(0x444444, 0.2))
scene.add(new HemisphereLight(0xffffff, 'gray', 0.8))
scene.add(new PointLight('white', 0.5))

let figures_white: Object3D[] = [];
for (let i =0; i < 8; i++){
    let figure_geom = new CylinderGeometry(0.4, 0.4, 0.3, 50)
    let figure_mat = new MeshPhongMaterial({color: 0x8B0000, shininess: 100, specular: 0x050505})
    let figure = new Mesh(figure_geom, figure_mat)
    figure.position.z = -4 + 0.4
    figure.position.x = -4 + i + 0.4
    figure.position.y = 0.16

    figures_white.push(figure)
    scene.add(figure)
}

let figures_black: Object3D[] = [];
for (let i =0; i < 8; i++){
    let figure_geom = new CylinderGeometry(0.4, 0.4, 0.3, 50)
    let figure_mat = new MeshPhongMaterial({color: 0x00FF00, shininess: 100, specular: 0x050505})
    let figure = new Mesh(figure_geom, figure_mat)
    figure.position.z = +4 - 0.4
    figure.position.x = -4 + i + 0.4
    figure.position.y = 0.16
    figures_black.push(figure)
    scene.add(figure)
}



let raycaster = new Raycaster();
let mousepos = new Vector2();

function onMouseMove( event: MouseEvent ) {

	// calculate mouse position in normalized device coordinates
	// (-1 to +1) for both components

	mousepos.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	mousepos.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

}
window.addEventListener( 'mousemove', onMouseMove, false );



function highlight(figures: Object3D[]) {
    raycaster.setFromCamera(mousepos, camera)
    let intersects = raycaster.intersectObjects(figures)

    for (let figure of figures){
        figure.position.y = 0.16
    }

    for (let intersect of intersects){
        if (figures.find(it => it === intersect.object) != undefined){
            intersect.object.position.y = 0.3
            highlighted = intersect.object
        }
    }
}

var highlighted: Object3D = null;
var selected: Object3D = null;

function select_figure() {
    selected === null ? selected = highlighted : selected = null;
    console.log("selected", selected)

}
window.onclick = select_figure

function move_figure(figure: Object3D){
    raycaster.setFromCamera(mousepos, camera)
    let intersects = raycaster.intersectObjects([board])
    if (intersects[0] != undefined){
        figure.position.set(intersects[0].point.x, intersects[0].point.y + 0.16, intersects[0].point.z)
    }
    window.onclick = (ev: Event)=> {
        if (intersects[0] != undefined) {
            figure.position.set(intersects[0].point.x, intersects[0].point.y + 0.16, intersects[0].point.z)
            selected = null
        }
    }
}


function animate() {
	requestAnimationFrame( animate );

    raycaster.setFromCamera(mousepos, camera)

    if (selected === null){
        window.onclick = select_figure
        highlight(figures_white)
        highlight(figures_black)
    } else {
        move_figure(selected)
    }
    renderer.render( scene, camera );

}
animate();